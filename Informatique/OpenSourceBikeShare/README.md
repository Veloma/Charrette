
[TOC]

# Introduction

En 2016, le logiciel OpenSourceBikeShare paraissait sur internet. Développé par une équipe de cyclistes et programmeurs slovaques son fonctionnement se voulait simple:

 * Une flotte de vélo quelconque
 * Utilisation de cadenas à code avec 4 chiffres
 * Echange de SMS avec un serveur pour obtenir le code actuel et le nouveau code de l'antivol
 * Une petite page web pour visualiser l'emplacement sur une carte
 * Un logiciel léger et simple qui peut être héberger chez soi


OpenSourceBikeShare a été utilisé à Bratislava et dans d'autres communes de Slovaquie mais à notre connaissance une seule autre personne maintient un parc à l'échelle du campus de son université.
Le logiciel, disponible sur [Github](https://github.com/cyklokoalicia/OpenSourceBikeShare), n'est plus à jour et en l'état le code n'est pas sécurisé, il ne répond plus aux normes actuelles.

Après des discussions avec le développeur principal de l'époque et la personne qui maintient un parc opérationnel aux Pays-Bas, nous avons souhaité porter le fonctionnement d'OSBS vers un module dédié au logiciel Dolibarr en ajoutant quelques fonctionnalités notamment la possibilité de réserver

Dolibarr est un logiciel libre de gestion d'entreprise et d'association, un ERP dans le jargon. Il peut être installé sur n'importe quel ordinateur ou serveur et est pensé pour être modulaire. Si nativement ses fonctionnalités répondent aux besoins principaux d'une association comme la gestion des adhérents et la gestion des devis et factures, il est possible d'ajouter des modules.

Dolibarr est utilisé par de nombreuses entreprises et est maintenu par une communauté de développeurs professionnels. Ainsi nous avons choisi de faire développer un module largement inspiré d'OpenSourceBikeShare qui à installer sur Dolibarr.

Pour le reste du document, nous supposerons que vous avez une instance de Dolibarr fonctionnelle.
Pour ce faire, voici de la documentation:

 * [Wiki de Dolibarr](https://wiki.dolibarr.org/index.php?title=Installation_-_Mise_%C3%A0_jour)
 * [Création d'une instance de test](https://www.dolibarr.org/onlinedemo.php)

Pour le fournisseur de SMS, nous utiliserons Octopush.
Pour 130€ HT par an vous aurez un numéro de téléphone dédié qui sera manipulable via une API et 300sms coûtent 18€ HT.

Donc avec 200€ TTC, si vous auto-héberger votre serveur et Dolibarr vous obtenez un service de location ou prêt de vélo.


# Le module
## Architecture

Il s'agit en réalité de 4 modules:

 * Module Bike, qui crée l'objet Vélo
 * Module Stand, qui crée l'objet Station
 * Module SMS, qui crée l'interface entre le fournisseur de SMS et Dolibarr
 * Module OpenSourceBikeShare, qui implémente toute la logique du système.

![Architecture de la refonte d'OpenSourceBikeShare](Informatique/OpenSourceBikeShare/img/architecture.png)


![Example de communication entre l'utilisateur et le système informatique](Informatique/OpenSourceBikeShare/img/communication.png)


## Installation

Télécharger sur votre station de travail les dernières versions des quatres modules accessibles dans le répertoire [modules](Informatique/OpenSourceBikeShare/modules)

Dans Dolibarr, rendez-vous dans "Configuration", "Modules et Applications", "Déployer/installer un module externe" puis téléverser l'un après l'autres les quatres archives

![Installation des modules pour Dolibarr](Informatique/OpenSourceBikeShare/img/config-dolibarr-ajout-modules.png)

Il reste à activer chacun des modules en allant dans "Module et applications", cherchez les modules Bike, Stand, Véloma et Octopush.


![Activation des modules](Informatique/OpenSourceBikeShare/img/config-dolibarr-activation-modules.png)

Il faut activer le module "API/Web services" aussi.

## Configuration

## Vélo et Stations

Dans la configuration des modules "Vélo" et "Station" il est possible de définir la forme que prendront les noms des vélos et stations. Par défaut c'est "V00X" et "S00Y" mais ça peut être modifié.

![Configuration module vélo](Informatique/OpenSourceBikeShare/img/config-dolibarr-module-bike.png)

![Configuration module station](Informatique/OpenSourceBikeShare/img/config-dolibarr-module-stand.png)

### Configuration d'Octopush et du module Octopush

Connectez-vous à Octopush et récupérez le login et clé d'API Octopush dans "API HTTP Identifiants".

![Login et clé API d'Octopush](Informatique/OpenSourceBikeShare/img/config-octopush-API.png)

Renseignez ces données dans la configuration du module Octopush de Dolibarr

![Configuration du module Octopush](Informatique/OpenSourceBikeShare/img/config-dolibarr-module-osbs.png)

Copiez la clé API d'un utilisateur qui a les droits d'accès sur les modules que nous utilisons en allant sur la page de l'utilisateur et cliquez sur l'intitulé "caché" pour copier la clé.

![Copie de la clé API Dolibarr](Informatique/OpenSourceBikeShare/img/config-dolibarr-dolapikey.png)

Dans l'interface d'Octopush, [https://client.octopush.com/api-callbacks](https://client.octopush.com/api-callbacks), renseignez l'adresse de callback suivante:

> https://VOTREDOMAINE/index.php/dolipushapi?DOLAPIKEY=xxxxxxxx

![Configuration de l'adresse de callback dans Octopush](Informatique/OpenSourceBikeShare/img/config-octopush.png)


### Configuration du module OpenSourceBikeShare

Sur la page de configuration de ce module, vous pourrez définir:

 * Le centre de la carte par défaut
 * Zoom initial, 13 par défaut
 * Nombre limite de vélo louable
 * L'utilisation ou non d'un système de crédit pour la location
    * Le nombre de crédit à l'ouverture d'un compte
    * Nombre de crédit consommé par unité de temps
    * Définition de l'unité de temps en minutes
    * Définition de la période gratuite en début de course
 * L'autorisation pour des utilisateurs non-enregistrés d'utiliser et de s'enregistrer en envoyant simplement un sms.

![Configuration du module OSBS](Informatique/OpenSourceBikeShare/img/config-dolibarr-module-osbs.png)

## Utilisation

Deux interfaces existent pour emprunter ou louer un cycle, par sms ou par une page web.

### SMS

Il existe 18 mots-clés pour dialoguer avec le système par sms, en voici la liste et comment les utiliser:

| Mot-clé     | Fonction                                                                                            | Message type                                |
| ----------- | --------------------------------------------------------------------------------------------------- | ------------------------------------------- |
| AIDE        | Pour obtenir la liste des mots-clés disponible                                                      | AIDE                                        |
| CREDIT      | Si crédit activé, renvoi crédit disponible                                                          | CREDIT                                      |
| LIBRE       | Envoi liste vélos disponibles dans toutes les stations                                              | DISPONIBLE                                  |
| LOUER       | Pour louer un cycle                                                                                 | LOUER velo                                  |
| RENDRE      | Pour ramener un cycle dans une station                                                              | RENDRE velo station                         |
| INFO        | Info sur une station                                                                                | INFO station                                |
| NOTE        | Pour ajouter une note sur un vélo                                                                   | NOTE velo                                   |
| TAG         | Pour annoncer des dégradations sur une station                                                      | TAG station                                 |
| LISTE       | Liste cycles d'une station                                                                          | LISTE station                               |
| OU          | Si admin, où se trouve un vélo                                                                      | OU velo                                     |
| QUI         | Si admin, qui utilise un vélo                                                                       | QUI velo                                    |
| SUPPRNOTE   | Si admin, pour supprimer la dernière note d’un cycle                                                | SUPPRNOTE velo                              |
| SUPPRTAG    | Si admin, pour supprimer la dernière dégradation d’une station                                      | SUPPRTAG                                    |
| AJOUTER     | Si admin ajouter utilisateur avec prénom, nom, courriel, numéro de téléphone et crédit (facultatif) | AJOUTER prenom nom email téléphone (credit) |
| REVENIR     | Si admin, annuler une location, on garde le même code antivol qu'avant                              | REVENIR velo                                |
| DERNIER     | Si admin, dernier utilisateur d'un vélo                                                             | DERNIER velo                                |
| FORCELOUER  | Si admin, pour louer un vélo même si inactif                                                        | FORCELOUER                                  |
| FORCERENDRE | Si admin, pour rendre un vélo même si inactif                                                       | FORCERENDRE                                 |

### Interface web

Elle est accessible depuis la page suivante: [https://DOMAINE/custom/veloma/public/](https://DOMAINE/custom/veloma/public)

Vous pourrez vous y authentifier pour accéder ensuite à la carte regroupant station et cycles disponibles.
Cette interface permet, modulo la présence d'un terminal informatique près d'une station, de louer un cycle ou de le réserver sans l'utilisation d'un téléphone.

![Interface web d'accueil](Informatique/OpenSourceBikeShare/img/interface-web.png)

Depuis cette page web il est possible en plus de réserver un cycle pour une période de temps données.
Le calendrier natif de Dolibarr recense l'ensemble des réservations, il est possible de l'exporter en utilisant un lien généré et accessible dans la configuration du module agenda.
Il sera ensuite possible d'importer cet agenda dans un logiciel comme Thunderbird sur ordinateur ou sur ordiphone.

![Agenda depuis interface admin](Informatique/OpenSourceBikeShare/img/agenda.png)

![Sélection des liens d'export de l'agenda](Informatique/OpenSourceBikeShare/img/agenda_export.png)

![Vue de l'import dans un logiciel comme Thunderbird.](Informatique/OpenSourceBikeShare/img/agenda-thunderbird.png)
