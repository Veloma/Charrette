# Développement d’un module FreeCAD pour la mécano-soudure

[TOC]

## Remerciements

Remerciements à [Quentin](https://www.malt.fr/profile/quentinplisson) et [Jonathan](https://freecad-france.com/) pour le développement de ce module!
La communauté FreeCAD se porte bien et il y a de nombreuses forces vives prêtes à aider.

La représentation de la Charrette faite avec FreeCAD se trouve dans le dossier modèle.

![La Charrette sous FreeCAD](img/freecad.png)


Ce qui suit a été rédigé par Quentin.


## Résumé du projet

###  Contexte et objectif

La mission de développement d’un module FreeCAD pour la mécano-soudure, s’inscrit dans un contexte global d’appropriation du logiciel par les acteurs de l’auto-construction et de l’open-source. La plupart de ces acteurs, dont l’association Cyclonomia fait partie, se rejoignent autour d’un besoin commun : réaliser des conceptions à base de profilés aciers standards et d’éléments de transmissions de mouvements. FreeCAD ne permet pas en l'état actuel de dessiner efficacement ce types de systèmes. Il fallait donc développer un module, programmé en langage Python, qui le permette et qui soit libre de droit.
Plus précisément, la mission consistait à optimiser la partie la plus chronophage du flux de travail, encadré en jaune Figure 1. Les différents outils, en rouge et en bleu dans le diagramme, présentent des défauts et ne s’accorde pas bien entre-eux. Dans ce contexte, notre objectif premier était de regrouper et améliorer les outils existants, puis en ajouter en fonction de ce qu’il manque.

![Flux de travail pour la conception de machine auto-constructible avec FreeCAD](img/fluxTravail.jpg)

### Parties prenantes

Pour répondre à cet objectif, une équipe projet de 3 personnes a été constituée avec Michel comme responsable projet ; moi-même (Quentin Plisson, ingénieur indépendant) comme développeur principal ; et Jonathan Wiedemann, expert FreeCAD, comme support au développement. Par la suite, je me suis concentré plutôt sur la partie front-end c.-à-d. l’interface utilisateur, et Jonathan sur la part back-end, c.-à-d. les fonctions de modélisation.

La communauté des utilisateurs FreeCAD a été d’une aide précieuse, en particulier Vincent Ballu, chaudronnier-soudeur, à l’origine de la macro WarehouseProfiles.py qui sera présentée plus loin.

## Résultats

### Regroupement des outils de dessin

L’atelier MetalWB regroupe des outils de dessin pour faciliter la création de contour 3D servant de base aux structures soudées. La fonction ‘Nouvelle Esquisse’ notamment, d’aboutir à des contours comme illustré ci-dessous. Il est possible de donner une inclinaison au esquisses, de les copier-coller ou encore de les déplacer manuellement.

![Utilisation de la fonction 'Nouvelle Esquisse' dans MetalWB](img/03.jpg)

Les fonctions de l’atelier Curves permette d’ajouter des lignes paramétriques. Les lignes d’esquisses précédemment crées peuvent servir de base pour appliquer les fonctions de division ou de ligne entre deux point comme Figure 3 ci-dessous.

![Fonctions de l'atelier Curves](img/04.jpg)

Une fonction de création d’un cube paramétrique a été ajouté pour reproduire rapidement des structure ayant cette forme.

### Outils de mécano-soudure

L’habillage des lignes d’esquisse se fait grâce à la fonction ‘WarehouseProfiles’, spécifique à l’atelier MetalWB. Elle se présente comme une boîte de dialogue, affichée ci-dessous, où l’utilisateur pourra définir entièrement le profilé. L’appui sur le bouton ‘OK’ va créer un ensemble de corps, où chaque corps représente un débit.

![Création d'une structure soudée avec la fonction 'WarehouseProfiles'](img/05.jpg)

Chacun des corps crées via WarehouseProfiles est paramétrable. Les paramètres sont accessible depuis les propriétés pour obtenir un résultat plus fin, comme illustré ci-dessous:

![Réglage des paramètres des profilés](img/06.jpg)

D’autres fonctions de mécano-soudure sont disponibles au stade expérimental : une fonction d’ajustement et d’étirement et des fonctions de gestion des coins (cf. Figure 6).

![Fonctions expérimentales](img/07.jpg)

### Collaboration avec les ateliers d’assemblage

La pièce de mécano-soudure pourra être assemblé avec d’autres éléments via les différents ateliers d’assemblages : A2plus, Asm3 ou Assembly4. Ci-dessous Figure 7, le porte-tout de l’Atelier Paysan a été modélisé via Assembly4 qui permet de faire des animations de mécanisme, ici selon l’angle des roues et de l’essieu.

![Assemblage avec Assembly4 et MetalWB](img/08.jpg)

Il est aussi possible de modéliser directement les éléments manquants via Part Design comme Figure 8 où le plateau et les roues du diable sont crée à même les tubes. Cette méthode peut-être utilisée en association de la précédente. Elle est particulièrement efficace si les éléments à ajoutés n’ont pas été modélisés ultérieurement et que l’on souhaite ajuster directement les dimensions à la pièce soudée.

![Assemblage avec Part Design et MetalWB](img/09.jpg)

### Accès à l’atelier : installation et contribution

Le code source du projet est disponible en open-source dans le dépôt Framagit de l’association Cyclonomie, URL : https://framagit.org/Veloma/freecad_metal_workbench. Les utilisateurs pourront le copier et le modifier pour participer à son amélioration. Il est possible de l’installer via le gestionnaire d’Addon en ajoutant le dépôt aux préférences.

![Ajout du dépôt de l’atelier dans FreeCAD](img/10.jpg)
