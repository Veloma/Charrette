[TOC]

Cette partie documente le code embarqué dans l'arduino qui pilote l'assistance électrique.

# Généralités

Le code est organisé selon une machine à état. Le passage d'un état à l'autre dépend de:

 * l'interrupteur de mise en marche
 * l'interrupteur de freinage
 * la vitesse et le sens de rotation de la roue
 * l'effort longitudinal appliqué au timon
 * le courant qui sort de la batterie

 L'idée principale pour régir l'assistance électrique est d'utiliser un PID pour asservir le signal gâchette envoyé au contrôleur à l'effort longitudinal lu dans le timon avec les jauges de déformation.
 La lecture de la vitesse et du sens de rotation de la roue sont nécessaires pour les changements d'état.
 Le boîtier de contrôle influe directement via des interrupteurs sur les changements d'état et les paramètres du PID pour le rendre plus ou moins doux.

## PID

Pour comprendre comment fonctionne les régulateurs PID, la page [wikipedia](https://en.wikipedia.org/wiki/PID_controller)

Dans le code du Charduino, nous utilisons la bibliothèque de [Brett Beauregard](https://playground.arduino.cc/Code/PIDLibrary/).

Les paramètres utilisés pour avec les moteurs type DD35 et un contrôleur 1200W en 48V:

|              | Kp | Ki | Kd  |
| ------------ | -- | -- | --- |
| Mode doux    | 1  | 2  | 0.1 |
| Mode nerveux | 1  | 4  | 0.1 |


## Wattmètre

### Tension

Un pont diviseur de tension est utilisé pour mesurer la tension aux bornes de la batterie. Cette information est utile pour limiter l'usage de l'assistance à une plage de tension et ainsi préserver la batterie de décharge profonde. Cette sécurité est censée être intégrée dans les contrôleurs de moteur, il s'agit là d'une redondance de sécurité.

### Courant

Une résistance de shunt est utilisée avec un amplificateur opérationnel pour mesurer le courant sortant de la batterie.

Cette information est utile dans le cas où le contrôleur se met en défaut malgré l'envoi d'une commande par le signal de la gâchette.

Ce cas peut se produire en montée et avec un chargement lourd. Le contrôleur n'arrive pas à faire tourner la roue, il envoie plus de courant mais rien n'y fait. Le contrôleur se met en défaut.
Pour détecter ce cas, qui se produit davantage en utilisant un moteur sans capteur à effet hall, il est nécessaire de lire le courant de sortie de la batterie.

### Puissance

On en déduit plus ou moins finement la puissance consommée par le système.

## Lecture vitesse et rotation de la roue

On utilise les capteurs à effet hall embarqués dans le moteur pour savoir dans quelle sens tourne la roue et à quelle vitesse.

# Fonctionnement du code

Initialisation / setup:

  * Ouverture port série
  * Cartouche de bienvenue
  * Initialisation des entrées/sorties
  * Initialisation des interruptions
  * Initialisation du PID
  * Initialisation du wattmètre
  * Initialisation des différents chronomètres

Boucle principale:

  * Mise à jour de la vitesse
  * Lecture de la valeur du capteur de force
  * Lecture de la tension et du courant de sortie de batterie
  * Gestion des interruptions du boîtier de contrôle
  * Gestion de la machine à état
  * Impression des messages de debug
  * Vérification de l'état du contrôleur
  * Envoi de l'ordre de la gâchette et du frein

7 états:

* Etat 0: Initialisation
* Etat 1: Attente avant départ
* Etat 2: Roulage
* Etat 3: En suspens
* Etat 4: Déccélération
* Etat 5: Freinage
* Etat 6: N/A
* Etat 7: Réinitialisation du capteur


![Machine à état](Informatique/documents/machine-a-etat.png)

![Boucle](Informatique/documents/boucle-charduino.png)

\clearpage

# Installation

Utiliser l'IDE Arduino pour installer le micrologiciel.

Bibliothèques requises:

~~~
#include <EEPROM.h>
#include <Chrono.h>
#include <SoftFilters.h>
#include <PID_v1.h>
#include <Adafruit_NeoPixel.h>
#include <HX711_ADC.h>
~~~
